
# BACKEND VITAMINAWEB

## Sobre o Projeto

Trata-se de um teste prático para a vaga de FullStack Sr. para a empresa Vitamina Web.<br>

Neste repositório encontra-se o código fonte do backend desenvolvido em Laravel<br>

## Requisitos

PHP >= 7.4<br>
MySQL >= 15.1 ou MariaDB >= 10.4<br>
Composer >= 2.1<br>

## Executando o projeto

1 - Faça o clone do projeto ou baixe o zip e descompacte na pasta desejada.<br>
2 - Rode o comando "composer install" para instalar as dependências<br>
3 - Configure o ip e porta do banco de dados no arquivo .env<br>
4 - Execute o comando "php artisan migrate --seed" para rodar as migrations e seeders<br>
3 - Execute o projeto utilizando o comando "php artisan serve"<br>
4 - O projeto irá executar na porta 8000