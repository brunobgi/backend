<?php

namespace Database\Seeders;

use App\Models\User;
use App\Models\Client;
use Illuminate\Support\Arr;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class OpportunitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 0; $i < 100; $i++) {
            DB::table('opportunities')->insert([
                'user_id'   => User::inRandomOrder()->pluck('id')->first(),
                'client_id' => Client::inRandomOrder()->pluck('id')->first(),
                'status'    => Arr::random(['Em aberto', 'Aprovada', 'Recusada', 'Perdida', 'Vencida']),
                'created_at' => now(),
            ]);
        }
    }
}
