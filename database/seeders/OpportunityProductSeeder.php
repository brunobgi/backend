<?php

namespace Database\Seeders;

use App\Models\Product;
use App\Models\Opportunity;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class OpportunityProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $opportunities = Opportunity::pluck('id');

        $opportunities->map(function ($opportunity) {
            for ($i = 0; $i < rand(1, 10); $i++) {
                DB::table('opportunity_product')->insert([
                    'opportunity_id' => $opportunity,
                    'product_id'     => Product::inRandomOrder()->pluck('id')->first(),
                ]);
            }
        });
    }
}
