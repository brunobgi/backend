<?php

use Illuminate\Support\Facades\Route;

Route::group(['prefix' => 'v1', 'middleware' => 'api'], function () {
    Route::prefix('auth')->group(base_path('routes/api/v1/auth.php'));
    Route::prefix('user')->group(base_path('routes/api/v1/user.php'));
    Route::prefix('client')->group(base_path('routes/api/v1/client.php'));
    Route::prefix('product')->group(base_path('routes/api/v1/product.php'));
    Route::prefix('opportunity')->group(base_path('routes/api/v1/opportunity.php'));
});
