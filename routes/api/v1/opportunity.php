<?php

use App\Http\Controllers\Api\v1\OpportunityController;
use Illuminate\Support\Facades\Route;

Route::apiResource('', OpportunityController::class)->parameters(['' => 'opportunity']);
