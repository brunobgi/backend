<?php

use App\Http\Controllers\Api\v1\ClientController;
use Illuminate\Support\Facades\Route;

Route::apiResource('', ClientController::class)->parameters(['' => 'client']);
