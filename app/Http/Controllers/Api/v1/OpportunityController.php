<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Http\Requests\OpportunityStoreUpdateRequest;
use App\Services\Api\v1\OpportunityService;
use Illuminate\Http\Request;

class OpportunityController extends Controller
{
    private $opportunityService;

    public function __construct(OpportunityService $opportunityService)
    {
        $this->middleware('auth.jwt');
        $this->opportunityService = $opportunityService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return $this->opportunityService->result($request->all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\OpportunityStoreUpdateRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(OpportunityStoreUpdateRequest $request)
    {
        return $this->opportunityService->save($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $this->opportunityService->find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\OpportunityStoreUpdateRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(OpportunityStoreUpdateRequest $request, $id)
    {
        return $this->opportunityService->update($request->all(), $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return $this->opportunityService->delete($id);
    }
}
