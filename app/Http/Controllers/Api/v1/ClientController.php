<?php

namespace App\Http\Controllers\Api\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\Api\v1\ClientService;
use App\Http\Requests\ClientStoreUpdateRequest;

class ClientController extends Controller
{
    private $clientService;

    public function __construct(ClientService $clientService)
    {
        $this->middleware('auth.jwt');
        $this->clientService = $clientService;
    }

    /**
     * Display a listing of the resource.
     *
     * @param \Illuminate\Http\Request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return (isset($request->paginate) && $request->paginate === 'false')
            ? $this->clientService->all()
            : $this->clientService->result();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\ClientStoreUpdateRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ClientStoreUpdateRequest $request)
    {
        return $this->clientService->save($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $this->clientService->find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\ClientStoreUpdateRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ClientStoreUpdateRequest $request, $id)
    {
        return $this->clientService->update($request->all(), $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return $this->clientService->delete($id);
    }
}
