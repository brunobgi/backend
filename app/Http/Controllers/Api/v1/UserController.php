<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Services\Api\v1\UserService;

class UserController extends Controller
{
    private $userService;

    public function __construct(UserService $userService)
    {
        $this->middleware('auth.jwt');
        $this->userService = $userService;
    }

    public function index()
    {
        return $this->userService->result();
    }
}
