<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Exceptions\{
    TokenExpiredException,
    TokenInvalidException
};

class JWTMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        try {
            JWTAuth::parseToken()->authenticate();
        } catch (\Exception $e) {
            if ($e instanceof TokenInvalidException) {
                return response()->json(['error' => __('Invalid token')], Response::HTTP_FORBIDDEN);
            } else if ($e instanceof TokenExpiredException) {
                return response()->json(['error' => __('Expired token')], Response::HTTP_FORBIDDEN);
            } else {
                return response()->json(['error' => __('Authorization token not found')], Response::HTTP_UNAUTHORIZED);
            }
        }
        return $next($request);
    }
}
