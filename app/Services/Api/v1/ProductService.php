<?php

namespace App\Services\Api\v1;

use App\Models\Product;
use Illuminate\Http\Response;

class ProductService
{
    private $product;

    public function __construct(Product $product)
    {
        $this->product = $product;
    }

    public function all()
    {
        return $this->product->all();
    }

    public function result(int $perPage = 10)
    {
        $products = $this->product->getResult($perPage);

        return response()->json($products,);
    }

    public function save(array $data)
    {
        $product = $this->product->create($data);

        return response()->json($product);
    }

    public function find($id)
    {
        $product = $this->product->with('opportunities')->find($id);

        if (!$product)
            return response()->json(['error' => __('Not found')], Response::HTTP_NOT_FOUND);

        return response()->json($product);
    }

    public function update(array $data,  $id)
    {
        $product = $this->product->find($id);

        if (!$product)
            return response()->json(['error' => __('Not found')], Response::HTTP_NOT_FOUND);

        $product->update($data);

        return response()->json($product);
    }

    public function delete($id)
    {
        $product = $this->product->with('opportunities')->find($id);

        if (!$product)
            return response()->json(['error' => __('Not found')], Response::HTTP_NOT_FOUND);

        if ($product->whereHas('opportunities'))
            return response()->json(['error' => __('Remove all dependent records first')], Response::HTTP_BAD_REQUEST);

        $product->delete();

        return response()->json(['success' => true], Response::HTTP_NO_CONTENT);
    }
}
