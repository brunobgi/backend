<?php

namespace App\Services\Api\v1;

use App\Models\Opportunity;
use Illuminate\Http\Response;

class OpportunityService
{
    private $opportunity;

    public function __construct(Opportunity $opportunity)
    {
        $this->opportunity = $opportunity;
    }

    public function result(array $data, int $perPage = 10)
    {
        $opportunities = $this->opportunity->getResult($data, $perPage);

        return response()->json($opportunities);
    }

    public function save(array $data)
    {
        $data = [
            'client_id' => $data['client'],
            'user_id'   => $data['user'],
            'status'    => $data['status'],
            'products'  => $data['products'],
        ];

        $opportunity = $this->opportunity->create($data);
        $opportunity->products()->sync($data['products']);
        $opportunity->save();

        return response()->json($opportunity);
    }

    public function find($id)
    {
        $opportunity = $this->opportunity->with(['user', 'client', 'products'])->find($id);

        if (!$opportunity)
            return response()->json(['error' => __('Not found')], Response::HTTP_NOT_FOUND);

        return response()->json($opportunity);
    }

    public function update(array $data, $id)
    {
        $data = [
            'client_id' => $data['client'],
            'user_id'   => $data['user'],
            'status'    => $data['status'],
            'products'  => $data['products'],
        ];

        $opportunity = $this->opportunity->find($id);

        if (!$opportunity)
            return response()->json(['error' => __('Not found')], Response::HTTP_NOT_FOUND);

        $opportunity->update($data);
        $opportunity->products()->sync($data['products']);
        $opportunity->save();

        return response()->json($opportunity);
    }

    public function delete($id)
    {
        $opportunity = $this->opportunity->find($id);

        if (!$opportunity)
            return response()->json(['error' => __('Not found')], Response::HTTP_NOT_FOUND);

        $opportunity->delete();

        return response()->json(['success' => true], Response::HTTP_NO_CONTENT);
    }
}
