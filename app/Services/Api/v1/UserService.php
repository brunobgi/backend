<?php

namespace App\Services\Api\v1;

use App\Models\User;

class UserService
{
    private $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function result()
    {
        $users = $this->user->all();

        return response()->json($users,);
    }
}
