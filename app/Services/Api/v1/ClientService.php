<?php

namespace App\Services\Api\v1;

use App\Models\Client;
use Illuminate\Http\Response;

class ClientService
{
    private $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    public function all()
    {
        return $this->client->all();
    }

    public function result(int $perPage = 10)
    {
        $clients = $this->client->getResult($perPage);

        return response()->json($clients,);
    }

    public function save(array $data)
    {
        $client = $this->client->create($data);

        return response()->json($client);
    }

    public function find($id)
    {
        $client = $this->client->with('opportunities')->find($id);

        if (!$client)
            return response()->json(['error' => __('Not found')], Response::HTTP_NOT_FOUND);

        return response()->json($client);
    }

    public function update(array $data,  $id)
    {
        $client = $this->client->find($id);

        if (!$client)
            return response()->json(['error' => __('Not found')], Response::HTTP_NOT_FOUND);

        $client->update($data);

        return response()->json($client);
    }

    public function delete($id)
    {
        $client = $this->client->find($id);

        if (!$client)
            return response()->json(['error' => __('Not found')], Response::HTTP_NOT_FOUND);

        if ($client->whereHas('opportunities'))
            return response()->json(['error' => __('Remove all dependent records first')], Response::HTTP_BAD_REQUEST);

        $client->delete();

        return response()->json(['success' => true], Response::HTTP_NO_CONTENT);
    }
}
