<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    protected $fillable = ['title', 'price'];

    public function getResult(int $perPage)
    {
        return $this->with('opportunities')->orderBy('id', 'DESC')->paginate($perPage);
    }

    public function opportunities()
    {
        return $this->belongsToMany(Opportunity::class);
    }
}
