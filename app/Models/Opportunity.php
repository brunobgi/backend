<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Opportunity extends Model
{
    use HasFactory;

    protected $fillable = ['client_id', 'user_id', 'status'];

    public function getResult(array $data, int $perPage)
    {
        $result = $this->with(['user', 'client', 'products']);

        if (isset($data['user_id']) && !empty($data['user_id'])) {
            $result->where('user_id', $data['user_id']);
        }

        if (isset($data['date']) && !empty($data['date'])) {
            $result->whereBetween('created_at', [$data['date'] . ' 00:00:00', $data['date'] . ' 23:59:59']);
        }

        return $result->orderBy('id', 'DESC')->paginate($perPage);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function client()
    {
        return $this->belongsTo(Client::class);
    }

    public function products()
    {
        return $this->belongsToMany(Product::class);
    }
}
