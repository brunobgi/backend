<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Client extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'email', 'phone'];

    public function getResult(int $perPage)
    {
        return $this->with('opportunities')->orderBy('id', 'DESC')->paginate($perPage);
    }

    public function opportunities()
    {
        return $this->hasMany(Opportunity::class);
    }
}
